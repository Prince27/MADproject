# **HYPERTEXT TRANFER PROTOCOL (HTTP):**
#### **What is HTTP?**
> ##### *The Hypertext Transfer Protocol (HTTP) is application-level protocol for collaborative, distributed, hypermedia information systems. It is the data communication protocol used to establish communication between client and server.*
> ##### *HTTP is TCP/IP based communication protocol, which is used to deliver the data like image files, query results, HTML files etc on the World Wide Web (WWW) with the default port is TCP 80. It provides the standardized way for computers to communicate with each other.*
<img src='https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png' width=50% >

#### **Features of http**
#### *There are three fundamental features that make the HTTP a simple and powerful protocol used for communication:*
> * ##### **HTTP is media independent:** *It specifies that any type of media content can be sent by HTTP as long as both the server and the client can handle the data content.*
> * ##### **HTTP is connectionless:** *It is a connectionless approach in which HTTP client i.e., a browser initiates the HTTP request and after the request is sent the client disconnects from server and waits for the response.*
> * ##### **HTTP is stateless:** *The client and server are aware of each other during a current request only. Afterwards, both of them forget each other. Due to the stateless nature of protocol, neither the client nor the  server can retain the information about different request across the web pages.*

#### *HTTP Requests*
> ##### *The request sent by the computer to a web server, contains all sorts of potentially interesting information; it is known as HTTP requests.*
> ##### *The HTTP request method indicates the method to be performed on the resource identified by the Requested URI (Uniform Resource Identifier). This method is case-sensitive and should be used in uppercase.*
>**Example**:
```
GET / HTTP/1.1
Host: xkcd.com
Accept: text/html
User-Agent: Mozilla/5.0 (Macintosh)  

```

##### *The HTTP request methods are:*
| Http Request | Description |
| ------ | ------ |
| GET| Asks to get the resource at the requested URL.  |
| POST | Asks the server to accept the body info attached. It is like GET request with extra info sent with the request. |
| HEAD | Asks the server to accept the body info attached. It is like GET request with extra info sent with the request. |
| TRACE | Asks for the loopback of the request message, for testing or troubleshooting. |
| PUT | Says to put the enclosed info (the body) at the requested URL. |
| DELETE | Says to delete the resource at the requested URL. |
| OPTIONS | Asks for a list of the HTTP methods to which the thing at the request URL can respond  |

#### **Http Response**
**response is made of 3 parts**
- > **Status line**
- > **HTTP header**
- > **Message body**
```
HTTP/1.1 200 OK
Date: Sat, 02 Apr 2011 21:05:05 GMT
Server: lighttpd/1.4.19
Content-Type: text/html

<html>
    <!-- ... HTML for the xkcd comic -->
</html>

```
##### *The server will send you send you different status codes depending on situation*
<img src='https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg' width=50%>

#### **InfluxDB Component Grafana**
> ##### *Grafana is a data visualization tool used to display and represent the data.*

#### *Communication architecture*

### *InfluxDB configuration in Shunya*

| Configuration | Description |
| ------- | ------- |
| Influx DB URL | url to the Influx DB instance.|
| Database Name | Name of the database in which the data gets stored. |

#### *The configurations need to be set manually by the user before using the API's*
#### *For example:*
#### *A sample JSON snippet should contain*

```
"influxdb" : {
    "influxdb url": "",
    "db name": "",
}
```

####  *Commonly configured parameters*
>#### *For example:*
>#### *A typical Influxdb configuration without security will look like this*

```
"test-db" : {
    "influxdb url": "http://148.251.91.253:8086/",
    "db name": "test-db",
}
```
#### *API's*

| *API* | *Description* | *Details* |
|---- | ---- | ---- |
| `newInfluxdb()`	|Create a new Influx DB Instance. |	[Read More](#newinfluxdb) |
| `writeDbInflux()` | Send Data to Influx DB. | [Read More](#writedbinflux) |

#### `newInfluxdb()`
>#### *Description* : Creates a new Influxdb Instance
#### *Parameters*
 + `name (char *)` - Name of the JSON object.

**Return-type** : influxdbObj

**Returns** : influxdbObj Instance for Influxdb

**Usage :**

For example: Lets say your JSON file looks like
```
"test-db" : {
    "influxdb url": "http://148.251.91.253:8086/",
    "db name": "test-db",
}
```
So the usage of the API's will be
```
influxdbObj testdb = newInfluxdb("test-db");
```
---

#### `writeDbInflux()`
>#### *Description* : Send Data to Influx DB.
#### *Parameters*
 + `obj(influxdbObj)` - the InfulxDB Instance
 + `data(char)` - the data to be sent to Influxdb in Influx db line format
 + `...`

**Return-type** : int8_t

**Usage :**
```
influxdbObj testdb = newInfluxdb("test-db");
writeDbInflux (testdb, "measurement,tag1,tag2 value1=%d,value2=%d %ld", temp, pressure, unix_timestamp);
```
---