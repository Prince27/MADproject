# Motor Anomaly Detection

This project deals with the collection, Transmission and visualisation of metrics such as vibration, sound and temperature of motors

## GitLab workflow
For people who needs a refresher on GitLab workflow : [GitLab Workflow](https://www.youtube.com/watch?v=enMumwvLAug)

## completing tasks 
tasks can be primarily of two types 
1. Study
1. Coding 

Start by Cloning the MAD repo. 

use the src folder to submit your code.  
use the doc folder to submit your documentation.  

After pushing these to your repo create a merge request.  
Use the issues for any doubts regarding the assigned task.  
Reach of out for help if you are stuck on a task with no progress for more than 30 mins.  
Whatever little work you have done in a day make sure to commit them and push to GitLab.  

More tasks will be added according to market requirements
